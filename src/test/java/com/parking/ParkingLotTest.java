package com.parking;

import com.parking.exception.ParkingException;
import com.parking.exception.ParkingFullException;
import com.parking.exception.ParkingNotFoundException;
import com.parking.model.Ticket;
import com.parking.model.Vehicle;
import com.parking.model.VehicleType;
import com.parking.places.Place;
import com.parking.places.PlaceEnum;
import com.parking.startegy.Receipt;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class ParkingLotTest {

    private Vehicle pulsar;

    private Vehicle baleno;

    private Vehicle bus;

    private Place place;

    @BeforeEach
    void setUp() throws ParkingFullException, ParkingException, IOException {
        pulsar = new Vehicle("TN-7227", VehicleType.MOTORCYCLE);
        baleno = new Vehicle("TN-1107",  VehicleType.CAR);
        bus = new Vehicle("TN-1108",  VehicleType.BUS);
        place = new Place(PlaceEnum.MALL,3,2,1);
    }

    @Test
    void shouldBeAbleToParkMotorCycle() throws ParkingFullException, ParkingException {

        Ticket ticket = place.parkAVehicle(pulsar);

        assertNotNull(ticket);
        assertEquals("TN-7227", ticket.vehicleNo());
        assertEquals(6, ticket.ticketNo());
    }

    @Test
    void shouldBeAbleToParkCar() throws ParkingFullException, ParkingException {

        Ticket ticket = place.parkAVehicle(baleno);

        assertNotNull(ticket);
        assertEquals("TN-1107", ticket.vehicleNo());
        assertEquals(3, ticket.ticketNo());
    }


    @Test
    void shouldBeAbleToParkBus() throws ParkingFullException, ParkingException {

        Ticket ticket = place.parkAVehicle(bus);

        assertNotNull(ticket);
        assertEquals("TN-1108", ticket.vehicleNo());
        assertEquals(2, ticket.ticketNo());
    }


   @Test
    void shouldNotAbleToParkAVehicleWhenNoSpotIsNotAvailable() throws ParkingFullException, ParkingException {
       place = new Place(PlaceEnum.MALL,3,2,0);
        var exceptionThrown = Assertions.assertThrows(ParkingFullException.class, () -> {
            Ticket ticket = place.parkAVehicle(pulsar);
        });
        Assertions.assertEquals("No MOTORCYCLE parking is space available.", exceptionThrown.getMessage());
    }

    @Test
    void notAbleToParkBusInStadium() throws ParkingException {
        place = new Place(PlaceEnum.STADIUM,0,2,1);
        var exceptionThrown = Assertions.assertThrows(ParkingException.class, () -> {
            Ticket ticket = place.parkAVehicle(bus);
        });
        Assertions.assertEquals("BUS vehicle not allowed to park", exceptionThrown.getMessage());
    }

    @Test
    void notAbleToParkBusInAirport() throws ParkingException {
        place = new Place(PlaceEnum.AIRPORT,0,2,1);
        var exceptionThrown = Assertions.assertThrows(ParkingException.class, () -> {
            Ticket ticket = place.parkAVehicle(bus);
        });
        Assertions.assertEquals("BUS vehicle not allowed to park", exceptionThrown.getMessage());
    }

    @Test
    void shouldBeAbleUnParkVehicleUsingItsParkingTicketInMall() throws ParkingNotFoundException, ParkingFullException, ParkingException {
        Ticket ticket = place.parkAVehicle(baleno);
        final long HOUR = 3600*1000;
        ticket.setEndTimeStamp(new Date(new Date().getTime() + 3 * HOUR));
        Receipt receipt = place.unParkAVehicle(ticket);

        Assertions.assertEquals(baleno.getVehicleNumber(), receipt.getParkedVehicle().getVehicleNumber());
        Assertions.assertEquals(baleno.getVehicleType(), receipt.getParkedVehicle().getVehicleType());
        Assertions.assertEquals(60, receipt.getCharges());
    }

    @Test
    void shouldBeAbleUnParkVehicleUsingItsParkingTicketInStadium() throws ParkingNotFoundException, ParkingFullException, ParkingException {
        final long HOUR = 3600*1000;
        place = new Place(PlaceEnum.STADIUM,3,2,1);
        Ticket ticket = place.parkAVehicle(baleno);
        ticket.setEndTimeStamp(new Date(new Date().getTime() + 12 * HOUR));
        Receipt receipt = place.unParkAVehicle(ticket);

        Assertions.assertEquals(baleno.getVehicleNumber(), receipt.getParkedVehicle().getVehicleNumber());
        Assertions.assertEquals(baleno.getVehicleType(), receipt.getParkedVehicle().getVehicleType());
        Assertions.assertEquals(380, receipt.getCharges());
    }

    @Test
    void shouldBeAbleUnParkVehicleUsingItsParkingTicketInAirport() throws ParkingNotFoundException, ParkingFullException, ParkingException {
        final long HOUR = 3600*1000;
        place = new Place(PlaceEnum.AIRPORT,3,2,1);
        Ticket ticket = place.parkAVehicle(baleno);
        ticket.setEndTimeStamp(new Date(new Date().getTime() + 36 * HOUR));
        Receipt receipt = place.unParkAVehicle(ticket);

        Assertions.assertEquals(baleno.getVehicleNumber(), receipt.getParkedVehicle().getVehicleNumber());
        Assertions.assertEquals(baleno.getVehicleType(), receipt.getParkedVehicle().getVehicleType());
        Assertions.assertEquals(200, receipt.getCharges());
    }

    @Test
    void shouldNotBeAbleUnParkVehicleWhichIsNotParked() throws ParkingFullException, ParkingException {
        final long HOUR = 3600*1000;
        var tkt = new Ticket(6, pulsar.getVehicleNumber(), pulsar.getVehicleType(), new Date());
        tkt.setEndTimeStamp(new Date(new Date().getTime() + 3 * HOUR));
        var exceptionThrown = Assertions.assertThrows(ParkingNotFoundException.class, () -> {
            Receipt receipt = place.unParkAVehicle(tkt);
        });

        Assertions.assertEquals("Unable to find to MOTORCYCLE parking.", exceptionThrown.getMessage());
    }

}