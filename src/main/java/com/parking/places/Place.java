package com.parking.places;

import com.parking.exception.ParkingException;
import com.parking.exception.ParkingFullException;
import com.parking.exception.ParkingNotFoundException;
import com.parking.model.Spot;
import com.parking.model.Ticket;
import com.parking.model.Vehicle;
import com.parking.model.VehicleType;
import com.parking.startegy.ParkingChargingStrategy;
import com.parking.startegy.Receipt;
import com.parking.startegy.impl.AirportParkingStrategy;
import com.parking.startegy.impl.MallParkingStrategy;
import com.parking.startegy.impl.StadiumParkingStrategy;

import static com.parking.places.PlaceEnum.*;

public class Place {

    private final PlaceEnum placeEnum;

    private final ParkingLot parkingLot;

    public Place(PlaceEnum placeEnum, int busSize, int carSize, int motorcycleSize) {
        this.placeEnum = placeEnum;
        this.parkingLot = new ParkingLot(busSize, carSize, motorcycleSize);
    }

    public Ticket parkAVehicle(Vehicle vehicle) throws ParkingFullException, ParkingException {
        this.validateVehicle(vehicle);
        Spot spot = this.parkingLot.getSpot(vehicle.getVehicleType());
        return spot.park(vehicle);
    }

    private void validateVehicle(Vehicle vehicle) throws  ParkingException{
        if(vehicle.getVehicleType() == VehicleType.BUS &&
                (this.placeEnum == AIRPORT || this.placeEnum == STADIUM)){
            throw new ParkingException(vehicle.getVehicleType());
        }
    }

    public Receipt unParkAVehicle(Ticket ticket) throws ParkingFullException, ParkingException, ParkingNotFoundException {
        Spot spot = this.parkingLot.getSpotBySpotNo(ticket.vehicleType(), ticket.spotNo());
        return spot.unPark(ticket, getParkingStrategy(placeEnum));
    }

    private ParkingChargingStrategy getParkingStrategy(PlaceEnum placeEnum) {
        switch (placeEnum) {
            case MALL -> {
                return new MallParkingStrategy();
            }
            case STADIUM -> {
                return new StadiumParkingStrategy();
            }
            case AIRPORT -> {
                return new AirportParkingStrategy();
            }
        }
        return null;
    }


}
