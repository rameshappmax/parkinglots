package com.parking.places;

import com.parking.exception.ParkingFullException;
import com.parking.exception.ParkingNotFoundException;
import com.parking.model.Spot;
import com.parking.model.VehicleType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ParkingLot {
    private Map<VehicleType, List<Spot>> vehicleSlots;

    public ParkingLot(int busSize, int carSize, int motorcycleSize) {
        this.vehicleSlots = new HashMap<>() {
            {
                put(VehicleType.MOTORCYCLE, getEmptySpots(motorcycleSize));
                put(VehicleType.CAR, getEmptySpots(carSize));
                put(VehicleType.BUS, getEmptySpots(busSize));
            }

        };
    }

    List<Spot> getEmptySpots(int size) {
        return IntStream.range(0, size)
                .mapToObj(x -> new Spot())
                .collect(Collectors.toList());
    }

    public Spot getSpot(VehicleType vehicleType) throws ParkingFullException {
        for (var spot : this.vehicleSlots.get(vehicleType)) {
            if (spot.isEmpty()) return spot;
        }
        throw new ParkingFullException(vehicleType);
    }

    public Spot getSpotBySpotNo(VehicleType vehicleType, long spotNo) throws ParkingNotFoundException {
        for (var spot : this.vehicleSlots.get(vehicleType)) {
            if (spot.getSpotNo() == spotNo) return spot;
        }
        throw new ParkingNotFoundException(vehicleType);
    }
}
