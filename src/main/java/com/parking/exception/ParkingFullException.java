package com.parking.exception;

import com.parking.model.VehicleType;

public class ParkingFullException extends Throwable {
    public ParkingFullException(VehicleType vehicleType) {
        super(String.format("No %s parking is space available.", vehicleType.toString()));
    }
}
