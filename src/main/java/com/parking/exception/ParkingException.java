package com.parking.exception;

import com.parking.model.VehicleType;

public class ParkingException extends Throwable {
    public ParkingException(VehicleType vehicleType) {
        super(String.format("%s vehicle not allowed to park", vehicleType.toString()));
    }

}
