package com.parking.exception;

import com.parking.model.VehicleType;

public class ParkingNotFoundException extends Throwable {
    public ParkingNotFoundException(VehicleType type) {
        super(String.format("Unable to find to %s parking.", type));
    }
}
