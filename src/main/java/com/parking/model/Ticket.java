package com.parking.model;

import com.parking.startegy.ParkingChargingStrategy;
import com.parking.startegy.Receipt;

import java.util.Date;

public class Ticket {

    private static long ticketCounter = 1;

    private  VehicleType type;
    private String vehicleNo;
    private long ticketNo = 0;
    private long spotNo = 0;
    private Date timeStamp;
    private Date endTimeStamp;
    private final boolean isPaid;

    public Ticket(long slotNo, String vehicleNo, VehicleType type, Date timeStamp) {
        this.type = type;
        this.ticketNo = Ticket.ticketCounter++;
        this.spotNo = slotNo;
        this.vehicleNo = vehicleNo;
        this.timeStamp = timeStamp;
        this.isPaid = false;
    }
    public VehicleType vehicleType() {
        return type;
    }
    public String vehicleNo() {
        return this.vehicleNo;
    }

    public long ticketNo() {
        return this.ticketNo;
    }

    public long spotNo() {
        return this.spotNo;
    }

    public boolean isPaid() {
        return isPaid;
    }

    public Date timeStamp() {
        return timeStamp;
    }

    public Date getEndTimeStamp() {
        return endTimeStamp;
    }

    public void setEndTimeStamp(Date endTimeStamp) {
        this.endTimeStamp = endTimeStamp;
    }

    public Receipt generateReceipt(Vehicle parkedVehicle, ParkingChargingStrategy parkingStrategy) {
        int parkedHours = this.getParkedHours(this.timeStamp(), this.getEndTimeStamp());
        return new Receipt() {
            @Override
            public Vehicle getParkedVehicle() {
                return parkedVehicle;
            }
            @Override
            public double getCharges() {
                return parkingStrategy.calculateCharges(parkedHours,parkedVehicle);
            }
        };

    }

    private int getParkedHours(Date startDate, Date endDate) {
        int MILLISECOND_IN_A_SECOND = 1000;
        int SECONDS_IN_A_HOUR = 3600;
        long secondsSpent = (endDate.getTime() - startDate.getTime()) / MILLISECOND_IN_A_SECOND;
        return (int) (secondsSpent / SECONDS_IN_A_HOUR);
    }

}
