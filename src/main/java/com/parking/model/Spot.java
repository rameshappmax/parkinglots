package com.parking.model;

import com.parking.startegy.ParkingChargingStrategy;
import com.parking.startegy.Receipt;

import java.util.Date;

public class Spot {

    private static long slotCounter = 1;
    private long spotNo;
    private boolean isEmpty;
    private Vehicle parkedVehicle;

    public Spot() {
        this.spotNo = slotCounter++;
        this.isEmpty = true;
        this.parkedVehicle = null;
    }

    public boolean isEmpty() {
        return isEmpty;
    }

    public long getSpotNo() {
        return spotNo;
    }

    public Ticket park(Vehicle vehicle) {
        this.isEmpty = false;
        this.parkedVehicle = vehicle;
        return generateTicket();
    }

    public Receipt unPark(Ticket ticket, ParkingChargingStrategy parkingChargingStrategy) {
        Receipt receipt = ticket.generateReceipt(parkedVehicle,parkingChargingStrategy);
        this.isEmpty = true;
        this.parkedVehicle = null;
        return receipt;
    }

    private Ticket generateTicket() {
        return new Ticket(this.spotNo,
                this.parkedVehicle.getVehicleNumber(),
                this.parkedVehicle.getVehicleType(), new Date());
    }

}
