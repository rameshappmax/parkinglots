package com.parking.startegy;

import com.parking.model.Vehicle;

public interface Receipt {
    Vehicle getParkedVehicle();
    double getCharges();
}
