package com.parking.startegy.impl;

import com.parking.model.Vehicle;
import com.parking.model.VehicleType;
import com.parking.startegy.ParkingChargingStrategy;

public class StadiumParkingStrategy implements ParkingChargingStrategy {
    @Override
    public double calculateCharges(int parkedHrs, Vehicle vehicle) {
        int parkingCharge = 0;
        if(vehicle.getVehicleType() == VehicleType.MOTORCYCLE){
            parkingCharge = getParkingChargeForMotorCycle(parkedHrs, parkingCharge);
        }
        if(vehicle.getVehicleType() == VehicleType.CAR){
            parkingCharge = getParkingChargeForCar(parkedHrs, parkingCharge);
        }
        return parkingCharge;
    }

    private int getParkingChargeForMotorCycle(int parkedHrs, int parkingCharge) {
        if(parkedHrs >= 0 && parkedHrs < 4){
            parkingCharge = 30;
        }
        if(parkedHrs >= 4 && parkedHrs < 12 ){
            parkingCharge = 90;
        }
        if(parkedHrs >= 12 ){
            parkingCharge = 90 + (parkedHrs - 11) * 100;
        }
        return parkingCharge;
    }

    private int getParkingChargeForCar(int parkedHrs, int parkingCharge) {
        if(parkedHrs >= 0 && parkedHrs < 4){
            parkingCharge = 60;
        }
        if(parkedHrs >= 4 && parkedHrs < 12 ){
            parkingCharge = 180;
        }
        if(parkedHrs >= 12 ){
            parkingCharge = 180 + (parkedHrs - 11) * 200;
        }
        return parkingCharge;
    }
}
