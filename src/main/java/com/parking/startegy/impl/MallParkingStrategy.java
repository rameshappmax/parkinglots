package com.parking.startegy.impl;

import com.parking.model.Vehicle;
import com.parking.model.VehicleType;
import com.parking.startegy.ParkingChargingStrategy;

public class MallParkingStrategy implements ParkingChargingStrategy {
    @Override
    public double calculateCharges(int parkedHrs, Vehicle vehicle) {
        if(vehicle.getVehicleType() == VehicleType.MOTORCYCLE){
            return parkedHrs * 10;
        }else if(vehicle.getVehicleType() == VehicleType.CAR){
            return parkedHrs * 20;
        }else if(vehicle.getVehicleType() == VehicleType.BUS){
            return parkedHrs * 50;
        }
        return 0;
    }
}
