package com.parking.startegy.impl;

import com.parking.model.Vehicle;
import com.parking.model.VehicleType;
import com.parking.startegy.ParkingChargingStrategy;

public class AirportParkingStrategy implements ParkingChargingStrategy {
    @Override
    public double calculateCharges(int parkedHrs, Vehicle vehicle) {
        int parkingCharge = 0;
        if(vehicle.getVehicleType() == VehicleType.MOTORCYCLE){
            parkingCharge = getParkingChargeForMotorCycle(parkedHrs, parkingCharge);
        }
        if(vehicle.getVehicleType() == VehicleType.CAR){
            parkingCharge = getParkingChargeForCar(parkedHrs, parkingCharge);
        }
        return parkingCharge;
    }

    private int getParkingChargeForMotorCycle(int parkedHrs, int parkingCharge) {
        if(parkedHrs >= 1 && parkedHrs < 8){
            parkingCharge = 40;
        }
        if(parkedHrs >= 8 && parkedHrs < 24 ){
            parkingCharge = 60;
        }
        if(parkedHrs >= 24 ){
            parkingCharge =  parkedHrs/24 * 80;
        }
        return parkingCharge;
    }

    private int getParkingChargeForCar(int parkedHrs, int parkingCharge) {
        if(parkedHrs >= 0 && parkedHrs < 12){
            parkingCharge = 60;
        }
        if(parkedHrs >= 12 && parkedHrs < 24 ){
            parkingCharge = 80;
        }
        if(parkedHrs >= 24 ){
            parkingCharge = parkedHrs/24 * 100;
        }
        return parkingCharge;
    }
}
