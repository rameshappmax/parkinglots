package com.parking.startegy;

import com.parking.model.Vehicle;

public interface ParkingChargingStrategy {
    double calculateCharges(int parkedHrs, Vehicle vehicle);
}
