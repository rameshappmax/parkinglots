import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class Application {

    //Please run the test case. Ignore this main method
    public static void main(String[] args) throws IOException {
        var application = new Application();
        application.readConfig();
    }

    void readConfig() throws IOException {
        File file = new File(this.getClass().getClassLoader().getResource("config.json").getFile());
        ObjectMapper objectMapper= new ObjectMapper();
        var data = objectMapper.readValue(file, Object.class);
    }

}
